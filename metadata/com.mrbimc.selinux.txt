Categories:System
License:GPLv3
Web Site:
Source Code:https://github.com/MrBIMC/SELinuxModeChanger
Issue Tracker:https://github.com/MrBIMC/SELinuxModeChanger/issues

Auto Name:SELinuxModeChanger
Summary:Set SELinux mode on boot
Description:
Sets SELinux into desired mode on each boot.
.

Repo Type:git
Repo:https://github.com/MrBIMC/SELinuxModeChanger

Build:3.0.1,31
    commit=1b341fe7fbb979f1ead671ed41f488c792f6d4ec
    subdir=app
    gradle=yes
    srclibs=RootShell@1.3
    rm=app/libs/*
    prebuild=cp -fR $$RootShell$$/src/com src/main/java/

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:3.0.1
Current Version Code:31

