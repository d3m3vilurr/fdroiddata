Categories:Office
License:FreeBSD
Web Site:
Source Code:https://github.com/jorgecastillo/KanaDrill
Issue Tracker:https://github.com/jorgecastillo/KanaDrill/issues

Auto Name:KanaDrill
Summary:Learn the Japanese kana
Description:
Learn the Japanese kana through repetition
.

Repo Type:git
Repo:https://github.com/jorgecastillo/KanaDrill

Build:1.0.9,9
    commit=5c36e8c30644cfc2b0a8e7946506cb31cbbacbf4
    target=android-19

Build:1.1.0,10
    commit=802769c4fc7862a2e5b6dfc42faf2ec47fa031a3
    subdir=app
    gradle=yes

Build:1.1.1,11
    commit=ecefb5d6e384c63c495138354e58ead90f0d52ee
    subdir=app
    gradle=yes

Build:1.1.3,13
    commit=14389871eeb7180135ee33295a77728c4264ab9c
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.1.3
Current Version Code:13

